#!/bin/bash

alias glog='git log --graph --color=always --pretty="format:%s%n%C(dim cyan)%h%C(reset) %C(dim)%cr by %an%C(reset)"'
alias gaff='git merge --ff-only'
alias getf='git fetch'
alias gulf='git pull --ff-only'
alias gout='git checkout'

function grog-fetched {
  fetch_head="$(git rev-parse --show-toplevel)/.git/FETCH_HEAD"
  if [[ -s ${fetch_head} ]]; then
    stat -f '%Sm' ${fetch_head}
  else
    echo 'Never'
  fi
}

function grog-tracking {
  if [[ $# -gt 0 ]] && [[ "$1" = '--remote' ]]; then
    grog-tracking | awk 'BEGIN { FS="/" } { print $1 }'
  elif [[ $# -gt 0 ]] && [[ "$1" = '--branch' ]]; then
    grog-tracking | awk 'BEGIN { FS="/" } { print $2 }'
  else
    git rev-parse --abbrev-ref --symbolic-full-name @{u} 2> /dev/null || echo 'NULL/NULL'
  fi
}

function grog-ahead {
  (git rev-list --count $(grog-tracking)..HEAD) 2> /dev/null
}

function grog-behind {
  (git rev-list --count HEAD..$(grog-tracking)) 2> /dev/null
}

function grog-tracking-info {
  echo
  echo "Last fetch: $(grog-fetched)"
  echo "$(grog-tracking --remote) $(grog-behind)  ||  $(grog-ahead) $(git symbolic-ref --short HEAD)"
  echo
}

function grog {
  if ! (git status > /dev/null 2>&1); then
    echo "Not a git repo."
    exit 1
  fi
  grog-tracking-info
  if (glog >/dev/null 2>&1); then
    glog --color=always | head -n 20
    echo
  fi
  git status --short
  echo
}

function grad {
  if ! (git status > /dev/null 2>&1); then
    echo "Not a git repo."
    exit 1
  fi
  git add "${@}"
  git status --short
}

function groc {
  if ! (git status > /dev/null 2>&1); then
    echo "Not a git repo."
    exit 1
  fi
  git commit "${@}"
  grog-tracking-info
}

function gush {
  if ! (git status > /dev/null 2>&1); then
    echo "Not a git repo."
    exit 1
  fi
  git push "${@}"
  grog-tracking-info
}
