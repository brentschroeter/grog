# Grog: Four-letter Git utilities.

Alpha: Under active development.

## Installation

Add the following to your .bash\_profile or .bash\_rc (replace path with your own): `source /path/to/grog.bash`.

## Core Commands

`grog`: Summarizes repo status, including recent log graph, time since last fetch, and uncommitted changes.  
`glog`: Grog-formatted log.  
`grad <options>`: Maps to `git add <options>` and shows the results.  
`groc <options>`: Maps to `git commit <options>` and shows the results.  
`gush <options>`: Maps to `git push <options>` and shows the results.
